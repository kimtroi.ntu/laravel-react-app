import React, { Component } from 'react';

class UpdateProduct extends Component {

    constructor(props) {
        super(props);
        /* Initialize the state. */
        this.state = {
            updateProduct: {
                title: '',
                description: '',
                price: 0,
                availability: 0
            }
        }

        //Boilerplate code for binding methods with `this`
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInput = this.handleInput.bind(this);
    }

    /* This method dynamically accepts inputs and stores it in the state */
    handleInput(key, e) {
        /*Duplicating and updating the state */
        var state = Object.assign({}, this.state.updateProduct);
        state[key] = e.target.value;
        this.setState({updateProduct: state });
    }
    /* This method is invoked when submit button is pressed */

    handleSubmit(e) {
        //preventDefault prevents page reload
        e.preventDefault();
        /*A call back to the onAdd props. The current
         *state is passed as a param
         */
        this.props.onUpdate(this.state.updateProduct);
    }

    render() {
        const divStyle = {
            /*Code omitted for brevity */ }

        return(
            <div className="card mb-3">
                <h2 className="card-header"> Update product </h2>
                <div className="card-body">
                    <form onSubmit={this.handleSubmit}>
                        <div className="form-group">
                            <label>Title:</label>
                            <input type="text" className="form-control" onChange={(e)=>this.handleInput('title',e)} />
                        </div>

                        <div className="form-group">
                            <label>Description:</label>
                            <input type="text" className="form-control" onChange={(e)=>this.handleInput('description',e)} />
                        </div>

                        <div className="form-group">
                            <label>Price:</label>
                            <input type="text" className="form-control" onChange={(e)=>this.handleInput('price',e)} />
                        </div>

                        <input type="submit" className="btn btn-primary" value="Update" />
                    </form>
                </div>
            </div>)
    }
}

export default UpdateProduct;