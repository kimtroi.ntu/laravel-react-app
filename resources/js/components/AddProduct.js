import React, { Component } from 'react';

class AddProduct extends Component {

    constructor(props) {
        super(props);
        /* Initialize the state. */
        this.state = {
            newProduct: {
                title: '',
                description: '',
                price: 0,
                availability: 0
            }
        }

        //Boilerplate code for binding methods with `this`
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInput = this.handleInput.bind(this);
    }

    /* This method dynamically accepts inputs and stores it in the state */
    handleInput(key, e) {
        /*Duplicating and updating the state */
        var state = Object.assign({}, this.state.newProduct);
        state[key] = e.target.value;
        this.setState({newProduct: state });
    }
    /* This method is invoked when submit button is pressed */

    handleSubmit(e) {
        //preventDefault prevents page reload
        e.preventDefault();
        /*A call back to the onAdd props. The current
         *state is passed as a param
         */
        this.props.onAdd(this.state.newProduct);
    }

    render() {
        const divStyle = {
            /*Code omitted for brevity */ }

        return(
            <div className="card">
                <h2 className="card-header"> Add new product </h2>
                <div className="card-body">
                    <form onSubmit={this.handleSubmit}>
                        <div className="form-group">
                            <label>Title:</label>
                            <input type="text" className="form-control" onChange={(e)=>this.handleInput('title',e)} />
                        </div>

                        <div className="form-group">
                            <label>Description:</label>
                            <input type="text" className="form-control" onChange={(e)=>this.handleInput('description',e)} />
                        </div>

                        <div className="form-group">
                            <label>Price:</label>
                            <input type="text" className="form-control" onChange={(e)=>this.handleInput('price',e)} />
                        </div>

                        <input type="submit" className="btn btn-primary" value="Add" />
                    </form>
                </div>
            </div>)
    }
}

export default AddProduct;