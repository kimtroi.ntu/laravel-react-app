import React, { Component } from 'react';

/* Stateless component or pure component
 * { product } syntax is the object destructing
 */
const Product = ({product, handleDelete}) => {

    //if the props product is null, return Product doesn't exist
    if(!product) {
        return(<div className="card mb-3"><div className="card-body">Product Doesnt exist</div></div>);
    }

    //Else, display the product data
    return(
        <div className="card mb-3">
            <h2 className="card-header"> {product.title} </h2>
            <div className="card-body">
                <p> {product.description} </p>
                <h3> Status {product.availability ? 'Available' : 'Out of stock'} </h3>
                <h3> Price : {product.price} </h3>
            </div>
            <div className="card-footer">
                <button className="btn btn-danger" onClick={() => handleDelete()}>Delete</button>
            </div>
        </div>
    )
}

export default Product ;