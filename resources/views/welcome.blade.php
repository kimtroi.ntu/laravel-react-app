<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Laravel React application</title>
        <link href="{{ mix('css/app.css') }}" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="jumbotron text-center">
            <h2> Laravel and React application </h2>
        </div>

        <div id="root" class="mb-5"></div>

        <div class="jumbotron text-center mb-0">
            <h2> Footer </h2>
        </div>

        <script src="{{ mix('js/app.js') }}" ></script>
    </body>
</html>
